# Changelog

This file reflects the evolution of the Athena Library, from one version to
another.

## Version 1.0.8

+ Wrap `ClassCastException` into `IllegalArgumentException`
+ Improve the documentation

## Version 1.0.7

+ Synchronize the access to the map used to store the receivers
+ Use 0-length arrays instead of `null` for method arguments
+ Remove a useless constructor for `RemoteMethod`
+ Use the declaring interface instead of the runtime class to retrieve the
  method to invoke
+ Improve the readability of the source code
+ Correct some linter issues

## Version 1.0.6

+ Change the access to the local address to handle multiple IP addresses 
  on the same machine

## Version 1.0.5

+ Replace the `"pong"` string returned by the default `Pinger` by the name
  of the server

## Version 1.0.4

+ Make easier the access to the `Logger` used by the library
+ Add the `Pinger` interface, with a default receiver for each `RpcServer`
+ Add the `ReceiverAccessor` interface, with a default receiver for each 
  `RpcServer`
+ Allow to use lambda expressions and method references as receivers

## Version 1.0.3

+ Allow an easy use of the first available port for servers

## Version 1.0.2

+ Use the existing `Supplier` functional interface instead of a custom one

## Version 1.0.1

+ New instantiation for `RpcRequest`

## Version 1.0

+ First implementation of `RpcServer`
+ First implementation of `RpcClient` with dynamic proxies
+ First implementation of requests and responses
+ First implementation of the RPC treatments
