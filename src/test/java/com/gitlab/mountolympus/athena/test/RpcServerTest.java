/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test;

import java.io.IOException;

import org.junit.Test;

import com.gitlab.mountolympus.athena.server.RpcServer;
import com.gitlab.mountolympus.athena.test.receivers.impl.AdderImpl;

/**
 * The RpcServerTest tests if only interfaces can be registered on an RpcServer.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */
public final class RpcServerTest {

    /**
     * Tests if an interface is required to register a receiver.
     * 
     * @throws IOException If the {@link RpcServer} could not be created.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testRegisterReceiver() throws IOException {
        try (RpcServer server = new RpcServer()) {
            server.registerReceiver(AdderImpl.class, new AdderImpl());
        }
    }

    /**
     * Tests if an interface is required to register a factory.
     * 
     * @throws IOException If the {@link RpcServer} could not be created.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testRegisterFactory() throws IOException {
        try (RpcServer server = new RpcServer()) {
            server.registerFactory(AdderImpl.class, AdderImpl::new);
        }
    }

}
