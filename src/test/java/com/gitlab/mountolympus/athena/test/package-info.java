/**
 * The com.gitlab.mountolympus.athena.test package contains the test cases of the Athena
 * library.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */

package com.gitlab.mountolympus.athena.test;
