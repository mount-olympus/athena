/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test.receivers.impl;

import com.gitlab.mountolympus.athena.test.receivers.Divider;

/**
 * The DividerImpl is an implementation of the {@link Divider} receiver interface.
 *
 * @author Romain WALLON
 */
public final class DividerImpl implements Divider {

    /*
     * (non-Javadoc)
     * 
     * @see com.gitlab.mountolympus.athena.test.receivers.Divider#divide(int, int)
     */
    @Override
    public int divide(int a, int b) {
        return a / b;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.gitlab.mountolympus.athena.test.receivers.Divider#divide(double, double)
     */
    @Override
    public double divide(double a, double b) {
        return a / b;
    }

}
