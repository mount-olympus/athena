/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.gitlab.mountolympus.athena.client.RpcClient;
import com.gitlab.mountolympus.athena.server.RpcServer;
import com.gitlab.mountolympus.athena.test.receivers.Divider;
import com.gitlab.mountolympus.athena.test.receivers.impl.DividerImpl;

/**
 * The DividerTest is the test case for a {@link Divider} receiver.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */
public final class DividerTest extends AbstractAthenaTest {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.gitlab.mountolympus.athena.test.AbstractAthenaTest#registerReceivers(com.gitlab
     * .mountolympus.athena.server.RpcServer)
     */
    @Override
    protected void registerReceivers(RpcServer rpcServer) {
        rpcServer.registerFactory(Divider.class, DividerImpl::new);
    }

    /**
     * Tests if the method
     * {@link com.gitlab.mountolympus.athena.test.receivers.Divider#divide(int, int)}
     * can be remotely called.
     */
    @Test
    public void testDivideInt() {
        Divider divider = RpcClient.createStub(HOST, port, Divider.class);
        assertEquals(12, divider.divide(24, 2));
    }

    /**
     * Tests if the method
     * {@link com.gitlab.mountolympus.athena.test.receivers.Divider#divide(int, int)}
     * does not allow to divide by 0.
     */
    @Test(expected = ArithmeticException.class)
    public void testDivideIntBy0() {
        Divider divider = RpcClient.createStub(HOST, port, Divider.class);
        divider.divide(1, 0);
    }

    /**
     * Tests if the method
     * {@link com.gitlab.mountolympus.athena.test.receivers.Divider#divide(double, double)}
     * can be remotely called.
     */
    @Test
    public void testDivideDouble() {
        Divider divider = RpcClient.createStub(HOST, port, Divider.class);
        assertEquals(12., divider.divide(24., 2.), 0.0001);
    }

    /**
     * Tests if the method
     * {@link com.gitlab.mountolympus.athena.test.receivers.Divider#divide(double, double)}
     * allows to divide by 0.
     */
    @Test
    public void testDivideDoubleBy0() {
        Divider divider = RpcClient.createStub(HOST, port, Divider.class);
        assertEquals(Double.POSITIVE_INFINITY, divider.divide(1., 0.), 0.0001);
    }

}
