/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.gitlab.mountolympus.athena.client.RpcClient;
import com.gitlab.mountolympus.athena.server.RpcServer;
import com.gitlab.mountolympus.athena.test.receivers.Adder;
import com.gitlab.mountolympus.athena.test.receivers.Divider;
import com.gitlab.mountolympus.athena.test.receivers.Pojo;
import com.gitlab.mountolympus.athena.test.receivers.impl.AdderImpl;
import com.gitlab.mountolympus.athena.test.receivers.impl.DividerImpl;
import com.gitlab.mountolympus.athena.treatment.RpcTreatmentException;

/**
 * The MultipleReceiversTest tests the use of multiple receivers on the same server.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */
public final class MultipleReceiversTest extends AbstractAthenaTest {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.gitlab.mountolympus.athena.test.AbstractAthenaTest#registerReceivers(com.gitlab
     * .mountolympus.athena.server.RpcServer)
     */
    @Override
    protected void registerReceivers(RpcServer rpcServer) {
        rpcServer.registerFactory(Adder.class, AdderImpl::new);
        rpcServer.registerFactory(Divider.class, DividerImpl::new);
    }

    /**
     * Tests if the methods
     * {@link com.gitlab.mountolympus.athena.test.receivers.Divider#divide(int, int)},
     * {@link com.gitlab.mountolympus.athena.test.receivers.Adder#add(int, int)} and
     * {@link com.gitlab.mountolympus.athena.test.receivers.Divider#divide(double, double)}
     * can be remotely called.
     */
    @Test
    public void testCallToDifferentReceivers() {
        RpcClient client = new RpcClient(HOST, port);
        Divider divider = client.createStub(Divider.class);
        Adder adder = client.createStub(Adder.class);

        assertEquals(12, divider.divide(24, 2));
        assertEquals(12., divider.divide(24., 2.), 0.0001);
        assertEquals(12, adder.add(5, 7));
    }

    /**
     * Tests if an {@link RpcTreatmentException} is produced when a non-registered
     * interface is remotely called.
     */
    @Test(expected = RpcTreatmentException.class)
    public void testExceptionWhenReceiverNotRegistered() {
        Pojo pojo = RpcClient.createStub(HOST, port, Pojo.class);
        pojo.getValue();
    }

}
