/**
 * The com.gitlab.mountolympus.athena.test.receivers.impl package contains some simple
 * receiver implementations used to test the Athena library.
 *
 * @author Romain WALLON
 */

package com.gitlab.mountolympus.athena.test.receivers.impl;
