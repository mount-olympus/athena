/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test.receivers;

/**
 * The Adder interface defines a simple receiver used to perform additions.
 *
 * @author Romain WALLON
 */
@FunctionalInterface
public interface Adder {

    /**
     * Adds the two given values.
     * 
     * @param a The first operand.
     * @param b The second operand.
     * 
     * @return The result of {@code a + b}.
     */
    int add(int a, int b);

}
