/**
 * The com.gitlab.mountolympus.athena.test.receivers package contains some simple
 * receiver interfaces used to test the Athena library.
 *
 * @author Romain WALLON
 */

package com.gitlab.mountolympus.athena.test.receivers;
