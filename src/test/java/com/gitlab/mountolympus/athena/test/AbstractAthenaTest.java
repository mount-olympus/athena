/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;

import com.gitlab.mountolympus.athena.server.RpcServer;

/**
 * The AbstractAthenaTest is the parent class of the test cases of the Athena library
 * which use receivers.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */
public abstract class AbstractAthenaTest {

    /**
     * The host of the server.
     */
    protected static final String HOST = "127.0.0.1";

    /**
     * The RPC server under test.
     */
    private RpcServer server;

    /**
     * The port of the server.
     */
    protected int port;

    /**
     * Starts a new RPC server before each test.
     * 
     * @throws IOException If the server could not be started.
     */
    @Before
    public void setUp() throws IOException {
        server = new RpcServer();
        port = server.getLocalPort();
        registerReceivers(server);
        new Thread(server::listenAndServe).start();
    }

    /**
     * Registers the tested receiver(s).
     * 
     * @param rpcServer The server on which the receiver(s) must be registered.
     */
    protected abstract void registerReceivers(RpcServer rpcServer);

    /**
     * Stops the RPC server after each test.
     * 
     * @throws IOException If the server could not be stopped.
     */
    @After
    public void tearDown() throws IOException {
        server.close();
    }

}
