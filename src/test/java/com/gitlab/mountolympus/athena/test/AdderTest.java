/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.gitlab.mountolympus.athena.client.RpcClient;
import com.gitlab.mountolympus.athena.server.RpcServer;
import com.gitlab.mountolympus.athena.test.receivers.Adder;

/**
 * The AdderTest is the test case for an {@link Adder} receiver.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */
public final class AdderTest extends AbstractAthenaTest {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.gitlab.mountolympus.athena.test.AbstractAthenaTest#registerReceivers(com.gitlab
     * .mountolympus.athena.server.RpcServer)
     */
    @Override
    protected void registerReceivers(RpcServer rpcServer) {
        rpcServer.registerReceiver(Adder.class, (a, b) -> a + b);
    }

    /**
     * Tests if the method
     * {@link com.gitlab.mountolympus.athena.test.receivers.Adder#add(int, int)}
     * can be remotely called.
     */
    @Test
    public void testAdd() {
        Adder adder = RpcClient.createStub(HOST, port, Adder.class);
        assertEquals(12, adder.add(5, 7));
    }

}
