/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import com.gitlab.mountolympus.athena.client.RpcClient;
import com.gitlab.mountolympus.athena.server.Pinger;
import com.gitlab.mountolympus.athena.server.ReceiverAccessor;
import com.gitlab.mountolympus.athena.server.RpcServer;

/**
 * The DefaultReceiversTest is the test case for the {@link Pinger} and the
 * {@link ReceiverAccessor} default receivers.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */
public final class DefaultReceiversTest extends AbstractAthenaTest {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.gitlab.mountolympus.athena.test.AbstractAthenaTest#registerReceivers(com.gitlab
     * .mountolympus.athena.server.RpcServer)
     */
    @Override
    protected void registerReceivers(RpcServer rpcServer) {
        // Nothing to do: only the default receivers are tested.
    }

    /**
     * Tests if the method
     * {@link com.gitlab.mountolympus.athena.server.Pinger#ping()}
     * can be remotely called.
     */
    @Test
    public void testPing() {
        Pinger pinger = RpcClient.createStub(HOST, port, Pinger.class);
        assertEquals("athena", pinger.ping());
    }

    /**
     * Tests if the method
     * {@link com.gitlab.mountolympus.athena.server.ReceiverAccessor#getReceivers()}
     * can be remotely called.
     */
    @Test
    public void testGetReceivers() {
        ReceiverAccessor receiverAccessor = RpcClient.createStub(HOST, port, ReceiverAccessor.class);
        Collection<Class<?>> receivers = receiverAccessor.getReceivers();
        assertEquals(2, receivers.size());
        assertTrue(receivers.contains(Pinger.class));
        assertTrue(receivers.contains(ReceiverAccessor.class));
    }

}
