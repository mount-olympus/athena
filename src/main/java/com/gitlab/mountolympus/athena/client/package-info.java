/**
 * The com.gitlab.mountolympus.athena.client package contains the classes used on the
 * client side of an RPC architecture, i.e. the "calling" side.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */

package com.gitlab.mountolympus.athena.client;
