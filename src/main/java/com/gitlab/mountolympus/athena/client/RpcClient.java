/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.client;

import java.lang.reflect.Proxy;
import java.net.URI;
import java.net.URL;

/**
 * The RpcClient is a factory which enables to create stubs for the interfaces remotely
 * implemented by an RPC server.
 * 
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
public final class RpcClient {

    /**
     * The host of the server.
     */
    private final String serverHost;

    /**
     * The port of the server.
     */
    private final int serverPort;

    /**
     * Creates a new RpcClient.
     * 
     * @param serverUrl The URL of the server.
     */
    public RpcClient(URL serverUrl) {
        this(serverUrl.getHost(), serverUrl.getPort());
    }

    /**
     * Creates a new RpcClient.
     * 
     * @param serverUri The URI of the server.
     */
    public RpcClient(URI serverUri) {
        this(serverUri.getHost(), serverUri.getPort());
    }

    /**
     * Creates a new RpcClient.
     * 
     * @param serverHost The host of the server.
     * @param serverPort The port of the server.
     */
    public RpcClient(String serverHost, int serverPort) {
        this.serverHost = serverHost;
        this.serverPort = serverPort;
    }

    /**
     * Creates a stub for the given interface.
     * Calling a method on it delegates the call to the remote server.
     * 
     * @param <T> The type of the stub.
     * 
     * @param declaringInterface The interface to create a stub for.
     * 
     * @return A stub for the given interface.
     * 
     * @throws IllegalArgumentException If {@code declaringInterface} is not an
     *         interface.
     */
    public <T> T createStub(Class<T> declaringInterface) {
        return createStub(serverHost, serverPort, declaringInterface);
    }

    /**
     * Creates a stub for the given interface.
     * Calling a method on it delegates the call to a remote server.
     * 
     * It is a shortcut to avoid to instantiate an {@link RpcClient} when a single
     * stub is needed.
     * 
     * @param <T> The type of the stub.
     * 
     * @param serverUrl The URL of the server.
     * @param declaringInterface The interface to create a stub for.
     * 
     * @return A stub for the given interface.
     * 
     * @throws IllegalArgumentException If {@code declaringInterface} is not an
     *         interface.
     */
    public static <T> T createStub(URL serverUrl, Class<T> declaringInterface) {
        return createStub(serverUrl.getHost(), serverUrl.getPort(), declaringInterface);
    }

    /**
     * Creates a stub for the given interface.
     * Calling a method on it delegates the call to a remote server.
     * 
     * It is a shortcut to avoid to instantiate an {@link RpcClient} when a single
     * stub is needed.
     * 
     * @param <T> The type of the stub.
     * 
     * @param serverUri The URI of the server.
     * @param declaringInterface The interface to create a stub for.
     * 
     * @return A stub for the given interface.
     * 
     * @throws IllegalArgumentException If {@code declaringInterface} is not an
     *         interface.
     */
    public static <T> T createStub(URI serverUri, Class<T> declaringInterface) {
        return createStub(serverUri.getHost(), serverUri.getPort(), declaringInterface);
    }

    /**
     * Creates a stub for the given interface.
     * Calling a method on it delegates the call to a remote server.
     * 
     * It is a shortcut to avoid to instantiate an {@link RpcClient} when a single
     * stub is needed.
     * 
     * @param <T> The type of the stub.
     * 
     * @param serverHost The host of the server.
     * @param serverPort The port of the server.
     * @param declaringInterface The interface to create a stub for.
     * 
     * @return A stub for the given interface.
     * 
     * @throws IllegalArgumentException If {@code declaringInterface} is not an
     *         interface.
     */
    @SuppressWarnings("unchecked")
    public static <T> T createStub(String serverHost, int serverPort, Class<T> declaringInterface) {
        return (T) Proxy.newProxyInstance(declaringInterface.getClassLoader(),
                new Class<?>[] { declaringInterface },
                new RemoteCallHandler(serverHost, serverPort, declaringInterface));
    }

}
