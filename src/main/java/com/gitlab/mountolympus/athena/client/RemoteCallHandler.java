/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.Socket;

import com.gitlab.mountolympus.athena.message.RpcRequest;
import com.gitlab.mountolympus.athena.message.RpcResponse;
import com.gitlab.mountolympus.athena.treatment.RemoteMethod;
import com.gitlab.mountolympus.athena.treatment.RpcTreatmentException;

/**
 * The RemoteCallHandler enables to perform remote call requests to a server.
 * It is internally used by stubs to delegate their work.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
final class RemoteCallHandler implements InvocationHandler {

    /**
     * The host of the RPC server.
     */
    private final String serverHost;

    /**
     * The port of the RPC server.
     */
    private final int serverPort;

    /**
     * The interface which is implemented on the server, and used to identify 
     * the receiver which will execute the method.
     */
    private final Class<?> declaringInterface;

    /**
     * Creates a new RemoteCallHandler.
     * 
     * @param serverHost The host of the server.
     * @param serverPort The port of the server.
     * @param declaringInterface The interface implemented on the server.
     */
    RemoteCallHandler(String serverHost, int serverPort, Class<?> declaringInterface) {
        this.serverHost = serverHost;
        this.serverPort = serverPort;
        this.declaringInterface = declaringInterface;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object,
     * java.lang.reflect.Method, java.lang.Object[])
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // Connecting to the RPC server.
        Socket socket = openConnection();
        
        try {
            // Creating and sending the request.
            RemoteMethod remoteMethod = new RemoteMethod(declaringInterface, method);
            Object[] concreteArguments = ((args == null) ? new Object[0] : args);
            String requestId = sendRequest(socket, remoteMethod, concreteArguments);

            // Receiving the response.
            RpcResponse response = receiveResponse(socket);
            if (!response.validate(requestId)) {
                throw new RpcTreatmentException("Invalid response.");
            }
            return response.getResult();
            
        } finally {
            // Closing the connection to the server.
            closeConnection(socket);
        }
    }

    /**
     * Opens the connection to the RPC server.
     * 
     * @return A socket to communicate with the server.
     * 
     * @throws RpcTreatmentException If the connection could not be established.
     */
    private Socket openConnection() {
        try {
            return new Socket(serverHost, serverPort);
            
        } catch (IOException e) {
            // A problem occurred while opening the socket.
            throw new RpcTreatmentException("Could not open connection to server.", e);
        }
    }

    /**
     * Sends the request to remotely call the given method with the given arguments.
     * 
     * @param socket The socket to send the request through.
     * @param remoteMethod The method to remotely call.
     * @param args The arguments to call the remote method with.
     * 
     * @return The identifier of the request.
     * 
     * @throws RpcTreatmentException If an error occurs while sending the request.
     */
    private String sendRequest(Socket socket, RemoteMethod remoteMethod, Object[] args) {
        try {
            RpcRequest request = RpcRequest.createFor(remoteMethod, args);
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            output.writeObject(request);
            return request.getId();

        } catch (IOException e) {
            // A problem occurred while sending the request.
            throw new RpcTreatmentException("Could not send request.", e);
        }
    }

    /**
     * Receives the response to an RPC request.
     * 
     * @param socket The socket to receive the response from.
     * 
     * @return The response to an RPC request previously sent.
     * 
     * @throws RpcTreatmentException If an error occurs while receiving the response.
     */
    private static RpcResponse receiveResponse(Socket socket) {
        try {
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
            return (RpcResponse) input.readObject();

        } catch (IOException | ClassNotFoundException e) {
            // A problem occurred while receiving the response.
            throw new RpcTreatmentException("Could not receive response.", e);
        }
    }

    /**
     * Closes the connection to the RPC server.
     * 
     * @param socket The socket used to communicate with the server.
     * 
     * @throws RpcTreatmentException If the connection could not be closed.
     */
    private void closeConnection(Socket socket) {
        try {
            socket.close();
            
        } catch (IOException e) {
            // A problem occurred while closing the socket.
            throw new RpcTreatmentException("Could not close connection to server.", e);
        }
    }

}
