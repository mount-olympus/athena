/**
 * The com.gitlab.mountolympus.athena.treatment package contains the classes used to
 * represent the treatment of a remote call.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */

package com.gitlab.mountolympus.athena.treatment;
