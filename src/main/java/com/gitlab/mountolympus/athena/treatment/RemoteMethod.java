/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.treatment;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * The RemoteMethod represents a method to call on a remote server.
 * 
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
public final class RemoteMethod implements Serializable {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = -8839742646823323431L;

    /**
     * The interface declaring the method to call.
     */
    private final Class<?> declaringInterface;

    /**
     * The name of the method to call.
     */
    private final String name;

    /**
     * The parameter types of the method to call.
     */
    private final Class<?>[] parameterTypes;

    /**
     * Creates a new RemoteMethod.
     * 
     * @param declaringInterface The interface declaring the method to call.
     * @param method The method to call.
     * 
     * @throws IllegalArgumentException If {@code declaringInterface} is not an interface.
     */
    public RemoteMethod(Class<?> declaringInterface, Method method) {
        if (!declaringInterface.isInterface()) {
            throw new IllegalArgumentException(declaringInterface + " is not an interface.");
        }

        this.declaringInterface = declaringInterface;
        this.name = method.getName();
        this.parameterTypes = method.getParameterTypes();
    }

    /**
     * Gives the interface declaring this method.
     *
     * @return The interface declaring this method.
     */
    public Class<?> getDeclaringInterface() {
        return declaringInterface;
    }

    /**
     * Reflectively invokes this method on the given target with the given arguments.
     * 
     * @param target The object on which this method must be invoked.
     * @param arguments The arguments to pass to this method for the invocation.
     * 
     * @return The result of the invocation.
     * 
     * @throws IllegalArgumentException If {@code target} is not an instance of the
     *         interface declaring this method.
     * @throws RpcTreatmentException If the reflective invocation fails.
     * @throws InvocationTargetException If the invocation produces an exception.
     */
    public Object invoke(Object target, Object[] arguments) throws InvocationTargetException {
        // Checking the type of the target.
        if (!declaringInterface.isInstance(target)) {
            throw new IllegalArgumentException("Invalid target object: " + target);
        }

        try {
            // Invoking the method.
            Method method = declaringInterface.getMethod(name, parameterTypes);
            method.setAccessible(true);
            return method.invoke(target, arguments);

        } catch (NoSuchMethodException | SecurityException | IllegalAccessException e) {
            // The reflective invocation has failed.
            throw new RpcTreatmentException("Error during reflexive invocation.", e);
        }
    }

}
