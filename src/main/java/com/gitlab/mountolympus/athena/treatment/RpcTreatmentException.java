/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.treatment;

import java.io.Serializable;

/**
 * The RpcTreatmentException is an {@link Exception} thrown when an error 
 * occurs while treating a remote procedure call.
 * 
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
public final class RpcTreatmentException extends RuntimeException {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = -3857941563479773804L;

    /**
     * Creates a new RpcTreatmentException.
     * 
     * @param message The message describing the problem.
     */
    public RpcTreatmentException(String message) {
        super(message);
    }

    /**
     * Creates a new RpcTreatmentException.
     * 
     * @param cause The exception that caused the new one.
     */
    public RpcTreatmentException(Throwable cause) {
        super(cause);
    }

    /**
     * Creates a new RpcTreatmentException.
     * 
     * @param message The message describing the problem.
     * @param cause The exception that caused the new one.
     */
    public RpcTreatmentException(String message, Throwable cause) {
        super(message, cause);
    }

}
