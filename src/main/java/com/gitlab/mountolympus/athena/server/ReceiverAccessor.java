/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.server;

import java.util.Collection;

/**
 * The ReceiverAccessor enables to get the collection of receivers that are
 * available on an {@link RpcServer}.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0.4
 */
@FunctionalInterface
public interface ReceiverAccessor {

    /**
     * Gives the receivers which have been registered on the server, given by 
     * their declaring interface.
     * 
     * @return The registered receivers.
     */
    Collection<Class<?>> getReceivers();

}
