/**
 * The com.gitlab.mountolympus.athena.server package contains the classes used on the
 * server side of an RPC architecture, i.e. the "execution" side.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 */

package com.gitlab.mountolympus.athena.server;
