/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.server;

import static com.gitlab.mountolympus.athena.message.RpcResponse.failure;
import static com.gitlab.mountolympus.athena.message.RpcResponse.success;
import static java.net.NetworkInterface.getNetworkInterfaces;
import static java.util.Collections.emptyList;
import static java.util.Collections.synchronizedMap;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.gitlab.mountolympus.athena.message.RpcRequest;
import com.gitlab.mountolympus.athena.message.RpcResponse;
import com.gitlab.mountolympus.athena.treatment.RemoteMethod;
import com.gitlab.mountolympus.athena.treatment.RpcTreatmentException;

/**
 * The RpcServer enables to receive RPC requests, call the needed methods and
 * then send back the result.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
public final class RpcServer implements Closeable {

    /**
     * The name of the library, used as default name.
     */
    private static final String ATHENA = "athena";

    /**
     * The logger used to log the exceptions.
     */
    private static final Logger LOGGER = Logger.getLogger(ATHENA);

    /**
     * The name of this server.
     */
    private final String name;

    /**
     * The server used to receive RPC requests.
     */
    private final ServerSocket server;

    /**
     * The factories to use to create the receivers.
     */
    private final Map<Class<?>, Supplier<?>> receiverFactories = synchronizedMap(new HashMap<>());

    /**
     * Creates a new RpcServer, which listens at the first available port.
     * A {@link Pinger} and a {@link ReceiverAccessor} are registered by default.
     * 
     * @throws IOException If an I/O error occurs when opening the underlying socket.
     * 
     * @since 1.0.3
     */
    public RpcServer() throws IOException {
        this(0);
    }

    /**
     * Creates a new RpcServer.
     * A {@link Pinger} and a {@link ReceiverAccessor} are registered by default.
     * 
     * @param port The port at which the server listens.
     * 
     * @throws IOException If an I/O error occurs when opening the underlying socket.
     */
    public RpcServer(int port) throws IOException {
        this(ATHENA, port);
    }

    /**
     * Creates a new RpcServer.
     * A {@link Pinger} and a {@link ReceiverAccessor} are registered by default.
     * 
     * @param name The name of the server.
     * @param port The port at which the server listens.
     * 
     * @throws IOException If an I/O error occurs when opening the underlying socket.
     * 
     * @since 1.0.5
     */
    public RpcServer(String name, int port) throws IOException {
        this.name = name;
        this.server = new ServerSocket(port);
        addDefaultReceivers();
    }

    /**
     * Creates a new RpcServer.
     * A {@link Pinger} and a {@link ReceiverAccessor} are registered by default.
     * 
     * @param port The port at which the server listens.
     * @param maxConnections The maximum number of allowed simultaneous connections.
     * 
     * @throws IOException If an I/O error occurs when opening the underlying socket.
     */
    public RpcServer(int port, int maxConnections) throws IOException {
        this(ATHENA, port, maxConnections);
    }

    /**
     * Creates a new RpcServer.
     * A {@link Pinger} and a {@link ReceiverAccessor} are registered by default.
     * 
     * @param name The name of the server.
     * @param port The port at which the server listens.
     * @param maxConnections The maximum number of allowed simultaneous connections.
     * 
     * @throws IOException If an I/O error occurs when opening the underlying socket.
     * 
     * @since 1.0.5
     */
    public RpcServer(String name, int port, int maxConnections) throws IOException {
        this.name = name;
        this.server = new ServerSocket(port, maxConnections);
        addDefaultReceivers();
    }

    /**
     * Adds the default receivers to this server.
     * 
     * @since 1.0.4
     */
    private void addDefaultReceivers() {
        registerReceiver(Pinger.class, () -> name);
        registerReceiver(ReceiverAccessor.class, () -> new HashSet<>(receiverFactories.keySet()));
    }

    /**
     * Gives all the IP addresses that identify this machine on the local
     * networks to which it is connected.
     * 
     * @return The IP addresses of this machine in the local networks.
     * 
     * @since 1.0.6
     */
    public List<InetAddress> getLocalAddresses() {
        try {
            // Browsing the network interfaces.
            List<InetAddress> addresses = new LinkedList<>();
            Enumeration<NetworkInterface> networkInterfaces = getNetworkInterfaces();

            while (networkInterfaces.hasMoreElements()) {
                // Retrieving the addresses of the current network interface.
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();

                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress.isSiteLocalAddress()) {
                        // This is a local address.
                        addresses.add(inetAddress);
                    }
                }
            }

            // Returning the addresses that have been found.
            return addresses;

        } catch (SocketException e) {
            // An error has occurred: returning an empty list.
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return emptyList();
        }
    }

    /**
     * Gives the port at which this server listens.
     * 
     * @return The port at which this server listens.
     * 
     * @since 1.0.3
     */
    public int getLocalPort() {
        return server.getLocalPort();
    }

    /**
     * Registers a receiver associated to an interface.
     * The {@code receiver} is always used when a method from
     * {@code declaringInterface} is remotely called.
     * 
     * @param <T> The type of the interface declaring the methods remotely callable.
     * @param <U> The type of the receiver.
     * 
     * @param declaringInterface The interface which declares the remotely callable
     *        methods.
     * @param receiver The receiver to use when a method from
     *        {@code declaringInterface} is called.
     * 
     * @throws IllegalArgumentException If {@code declaringInterface} is not an
     *         interface.
     */
    public <T, U extends T> void registerReceiver(Class<T> declaringInterface, U receiver) {
        registerFactory(declaringInterface, () -> receiver);
    }

    /**
     * Registers a receiver factory associated to an interface.
     * The receiver to use is created thanks to {@code factory} when a method
     * from {@code declaringInterface} is remotely called.
     * 
     * @param <T> The type of the interface declaring the methods remotely callable.
     * @param <U> The type of the receiver.
     * 
     * @param declaringInterface The interface which declares the remotely callable
     *        methods.
     * @param factory The factory used to create the appropriate receiver when
     *        a method from {@code declaringInterface} is called.
     * 
     * @throws IllegalArgumentException If {@code declaringInterface} is not an
     *         interface.
     */
    public <T, U extends T> void registerFactory(Class<T> declaringInterface, Supplier<U> factory) {
        if (!declaringInterface.isInterface()) {
            // Only interfaces can be registered.
            throw new IllegalArgumentException(declaringInterface + " is not an interface.");
        }

        receiverFactories.put(declaringInterface, factory);
    }

    /**
     * Removes a receiver from this server.
     * 
     * @param declaringInterface The class to remove the receiver of.
     * 
     * @since 1.0.4
     */
    public void unregister(Class<?> declaringInterface) {
        receiverFactories.remove(declaringInterface);
    }

    /**
     * Starts to receive and to treat RPC requests.
     */
    public void listenAndServe() {
        while (!server.isClosed()) {
            try {
                Socket client = server.accept();
                handleClient(client);

            } catch (IOException e) {
                LOGGER.log(Level.INFO, e.getMessage(), e);
            }
        }
    }

    /**
     * Handles the request sent by the client through the given socket.
     * The treatment is performed in an other thread.
     * 
     * @param socket The socket connecting the server to the client.
     */
    private void handleClient(Socket socket) {
        new Thread(() -> {
            try (Socket client = socket) {
                treatRequest(client);

            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Could not close communication socket.", e);
            }
        }).start();

    }

    /**
     * Treats the request which has been sent through the given socket.
     * 
     * @param socket The socket used to communicate between this server
     *        and its client.
     */
    private void treatRequest(Socket socket) {
        try {
            RpcRequest request = receiveRequest(socket);
            RpcResponse response = computeResponse(request);
            sendResponse(socket, response);

        } catch (RpcTreatmentException e) {
            // An exception occurred while treating the request.
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            sendResponse(socket, failure(null, e));

        } catch (Exception e) {
            // An unexpected exception occurred.
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            sendResponse(socket, failure(null, new RpcTreatmentException(e)));
        }
    }

    /**
     * Receives an RpcRequest from the given socket.
     * 
     * @param socket The socket to receive the request from.
     * 
     * @return The received request.
     * 
     * @throws RpcTreatmentException If an error occurs while receiving the request.
     */
    private static RpcRequest receiveRequest(Socket socket) {
        try {
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
            return (RpcRequest) input.readObject();

        } catch (IOException | ClassNotFoundException e) {
            // A problem occurred while receiving the request.
            throw new RpcTreatmentException("Could not receive request.", e);
        }
    }

    /**
     * Calls the method asked by the request, and returns its response.
     * 
     * @param request The request remotely calling a method on this server.
     * 
     * @return The response to the request.
     */
    private RpcResponse computeResponse(RpcRequest request) {
        RemoteMethod remoteMethod = request.getRemoteMethod();
        Class<?> declaringInterface = remoteMethod.getDeclaringInterface();

        // Retrieving the factory.
        Supplier<?> factory = receiverFactories.get(declaringInterface);
        if (factory == null) {
            // The interface has not been registered.
            LOGGER.log(Level.WARNING, "Call to non-registered interface: {0}", declaringInterface);
            return failure(request.getId(), new RpcTreatmentException("Unknown " + declaringInterface));
        }

        try {
            // Invoking the method, and returning the result.
            Object result = remoteMethod.invoke(factory.get(), request.getArguments());
            return success(request.getId(), result);

        } catch (InvocationTargetException e) {
            // Propagating the exception produced by the invocation.
            LOGGER.log(Level.INFO, e.getCause().getMessage(), e);
            return failure(request.getId(), e.getCause());
        }
    }

    /**
     * Sends the response to a request that has been treated.
     * 
     * @param socket The socket to send the response through.
     * @param response The response to send.
     */
    private static void sendResponse(Socket socket, RpcResponse response) {
        try {
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            output.writeObject(response);

        } catch (IOException e) {
            // A problem occurred while sending the response.
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        if (!server.isClosed()) {
            server.close();
        }
    }

}
