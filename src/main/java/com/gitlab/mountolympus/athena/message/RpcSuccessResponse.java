/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.message;

import java.io.Serializable;
import java.util.Objects;

/**
 * The RpcSuccessResponse represents a response to a remote procedure call
 * that has succeeded.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
final class RpcSuccessResponse extends RpcResponse {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 7398567696777700889L;

    /**
     * The result of the remote call.
     */
    private final Serializable result;

    /**
     * Creates a new RpcSuccessResponse.
     * 
     * @param id The identifier of the response.
     * @param result The result of the remote call.
     * 
     * @throws NullPointerException If {@code id} is {@code null}.
     */
    RpcSuccessResponse(String id, Serializable result) {
        super(Objects.requireNonNull(id, "Successful response cannot have a null identifier."));
        this.result = result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.gitlab.mountolympus.athena.message.RpcResponse#getResult()
     */
    @Override
    public Object getResult() {
        return result;
    }

}
