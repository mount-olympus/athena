/**
 * The com.gitlab.mountolympus.athena.message package contains the classes used to
 * represent the messages which are exchanged to perform remote procedure calls.
 * 
 * @author Romain WALLON
 *
 * @version 1.0.8
 */

package com.gitlab.mountolympus.athena.message;
