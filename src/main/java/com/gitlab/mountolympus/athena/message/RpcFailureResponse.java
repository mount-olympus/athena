/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.message;

import java.io.Serializable;
import java.util.Objects;

/**
 * The RpcFailureResponse represents a response to a remote procedure call 
 * that has failed.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
final class RpcFailureResponse extends RpcResponse {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 610136696252150464L;
    
    /**
     * The throwable that caused the failure.
     */
    private final Throwable failureCause;

    /**
     * Creates a new RpcFailureResponse.
     * 
     * @param id The identifier of the response.
     * @param failureCause The throwable that caused the failure.
     * 
     * @throws NullPointerException If {@code failureCause} is {@code null}.
     */
    RpcFailureResponse(String id, Throwable failureCause) {
        super(id);
        this.failureCause = Objects.requireNonNull(failureCause, "Failure cause cannot be null.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.gitlab.mountolympus.athena.message.RpcResponse#validate(java.lang.String)
     */
    @Override
    public boolean validate(String identifier) {
        return super.validate(identifier) || (getId() == null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.gitlab.mountolympus.athena.message.RpcResponse#getResult()
     */
    @Override
    public Object getResult() throws Throwable {
        // Propagating the failure by throwing its cause.
        throw failureCause;
    }

}
