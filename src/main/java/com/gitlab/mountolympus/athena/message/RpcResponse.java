/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.message;

import java.io.Serializable;

/**
 * The RpcResponse represents a response to a remote procedure call.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
public abstract class RpcResponse extends RpcMessage {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 5903570278985360065L;

    /**
     * Creates a new RpcResponse.
     * 
     * @param id The identifier of the response.
     */
    RpcResponse(String id) {
        super(id);
    }
    
    /**
     * Creates an RpcResponse for a remote call that has succeeded.
     * 
     * @param id The identifier of the response.
     * @param result The result of the call, which must be {@link Serializable}.
     * 
     * @return The response to the remote call.
     * 
     * @throws NullPointerException If {@code id} is {@code null}.
     * @throws IllegalArgumentException If {@code result} is not {@link Serializable}.
     */
    public static final RpcResponse success(String id, Object result) {
        try {
            return new RpcSuccessResponse(id, (Serializable) result);
            
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Response result is not Serializable.", e);
        }
    }

    /**
     * Creates an RpcResponse for a remote call that has failed.
     * 
     * @param id The identifier of the response.
     * @param failureCause The throwable that caused the failure.
     * 
     * @return The response to the remote call.
     * 
     * @throws NullPointerException If {@code failureCause} is {@code null}.
     */
    public static final RpcResponse failure(String id, Throwable failureCause) {
        return new RpcFailureResponse(id, failureCause);
    }
    
    /**
     * Checks if this response is valid with respect to the given identifier.
     * 
     * @param id The expected identifier of this response.
     * 
     * @return If this response is valid.
     * 
     * @throws NullPointerException If {@code id} is {@code null}.
     */
    public boolean validate(String id) {
        return id.equals(getId());
    }
    
    /**
     * Gives the result of the remote call.
     * 
     * @return The result of the remote call.
     * 
     * @throws Throwable If the remote call has thrown any {@link Throwable}.
     */
    public abstract Object getResult() throws Throwable;

}
