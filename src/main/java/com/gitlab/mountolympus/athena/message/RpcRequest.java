/**
 * (c) Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.athena.message;

import java.io.Serializable;
import java.util.UUID;

import com.gitlab.mountolympus.athena.treatment.RemoteMethod;

/**
 * The RpcRequest represents a request used to perform a remote procedure call.
 *
 * @author Romain WALLON
 *
 * @version 1.0.8
 * @since 1.0
 */
public final class RpcRequest extends RpcMessage {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1288489831971404253L;

    /**
     * The remote method to call.
     */
    private final RemoteMethod remoteMethod;

    /**
     * The arguments to call the method with.
     */
    private final Serializable[] arguments;

    /**
     * Creates a new RpcRequest.
     * 
     * @param id The identifier of the message.
     * @param remoteMethod The remote method to call.
     * @param arguments The arguments to call the method with.
     */
    private RpcRequest(String id, RemoteMethod remoteMethod, Serializable[] arguments) {
        super(id);
        this.remoteMethod = remoteMethod;
        this.arguments = arguments;
    }

    /**
     * Creates a new RpcRequest.
     * A random identifier is assigned to the created message.
     * 
     * @param remoteMethod The remote method to call.
     * @param arguments The arguments to call the method with, which must all be 
     *        {@link Serializable}.
     *        An empty array indicates that there is no argument.
     * 
     * @return The created RpcRequest.
     * 
     * @throws IllegalArgumentException If one of the arguments is not {@link Serializable}.
     */
    public static RpcRequest createFor(RemoteMethod remoteMethod, Object[] arguments) {
        try {
            // Casting the given arguments to Serializable ones.
            Serializable[] serializableArguments = new Serializable[arguments.length];
            for (int i = 0; i < arguments.length; i++) {
                serializableArguments[i] = (Serializable) arguments[i];
            }
            
            // Creating the request.
            return new RpcRequest(UUID.randomUUID().toString(), remoteMethod, serializableArguments);
            
        } catch (ClassCastException e) {
            // One of the casts has failed, so one argument does not implement Serializable.
            throw new IllegalArgumentException("One of the arguments is not Serializable.", e);
        }
    }

    /**
     * Gives the remote method to call.
     *
     * @return The remote method to call.
     */
    public RemoteMethod getRemoteMethod() {
        return remoteMethod;
    }

    /**
     * Gives the arguments to call the method with.
     *
     * @return The arguments to call the method with.
     */
    public Object[] getArguments() {
        return arguments;
    }

}
