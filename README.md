# Athena - A Library for Remote Procedure Calls in Java

[![build status](https://gitlab.com/mount-olympus/athena/badges/master/build.svg)](https://gitlab.com/mount-olympus/athena/commits/master)

## Description

The **Athena Library** allows to easily write a program based on Remote 
Procedure Calls in Java 1.8 or above.

As you may know, Java already provides an implementation of RPC, called Remote
Method Invocation (RMI). You can see its Javadoc
[here](https://docs.oracle.com/javase/8/docs/api/java/rmi/package-summary.html).
However, we think that this implementation is not easy to use.

Indeed, with RMI, you need to think of the remote use of your code **while**
writing it, by extending an interface and throwing dedicated checked 
exceptions.
This is **not** the spirit of RPC: you should be able to write your program as
usual, especially if you want to reuse it locally.

With Athena, all you need is to make the exchanged objects implement
`Serializable`.

Moreover, Athena provides an easy way to create your client stubs and register
your receivers on an RPC server.

> **Important Note**
>
> Because it does not work like Java's RMI, you cannot use Athena together
> with RMI.
> If you want to use Athena, you need to use it on **both sides** of your
> application.

## Overview

The following sections describe how to use the library.
You can also browse its [Javadoc](https://mount-olympus.gitlab.io/athena/),
or have a look to our test cases.

### Example

To use Athena, you need to define the contract of the receivers thanks to
**interfaces**.

In the following sections, we suppose that the `Divider` interface is 
available on both the client side and the server side.

```java
public interface Divider {

    int divide(int a, int b);

}
```

We also suppose that the following implementation of `Divider` is available
on the server side.

```java
public class DividerImpl implements Divider {

    @Override
    public int divide(int a, int b) {
        return a / b;
    }

}
```

Finally, the server is supposed to be hosted at `1.2.3.4`, port `5678`.

### *Client* side

The client only needs to know the interface.
No implementation is required.

To create a *stub* on the client side, it's really easy.
The following lines of code give you an instance of `Divider`,
that you can use as if the object were stored locally.
However, calling a method on it leads to an execution on the server.

If an exception is thrown by the method execution on the server, it is
propagated to the client.

```java
Divider divider = RpcClient.createStub("1.2.3.4", 5678, Divider.class);
int n = divider.divide(4, 2); // n is now equal to 2.
int m = divider.divide(4, 0); // Produces an ArithmeticException.
```

If you need to create multiple stubs connected to a same RPC server, you can
instantiate an `RpcClient`, to avoid to repeat the host and the port at each
call.
This is especially useful when the server hosts multiple receivers.

```java
RpcClient client = new RpcClient("1.2.3.4", 5678);
Divider divider = client.createStub(Divider.class);
int n = divider.divide(12, 2);
```

> *Note*
>
> If an unexpected exception is thrown by the server during the remote call,
> e.g. if the server fails to read the request or to find the appropriate
> receiver, or if the communication with the server fails, an 
> `RpcTreatmentException` is thrown on the client side when calling the method.
>
> This exception is unchecked, so you can just ignore it.
> However, if you want your code to be robust, you should not!

### *Server* side

On the server side, you need both the interface and a concrete implementation.

An `RpcServer` is a server that you can start (by calling `listenAndServe`) 
and stop (by calling `close`).
**You cannot start an `RpcServer` twice**.

Before starting your `RpcServer`, you need to register your **receivers** (the
concrete implementations of the interfaces).
You can only register a single implementation for a given interface, but as 
many interfaces as you want.
You can also remove receivers by calling `unregister`.

By default, two receivers are registered on a server (which can be unregistered
if needed):

+ A `Pinger`, which enables to check if the server is reachable, and returns 
  the name of the server (by default, this name is `"athena"`)
+ A `ReceiverAccessor`, which gives to the client the set of receivers that 
  are available (given by their declaring interface)

You can register an **instance** of the implementation, which will be used for
each call (for a stateful API, for instance), or a **factory**, which will be
used to create an instance of your receiver for each call (for a stateless API,
for instance).

Then, all you need is to start the server.

```java
RpcServer server = new RpcServer(5678);
server.registerReceiver(Divider.class, new DividerImpl()); // Stateful
server.registerFactory(Divider.class, DividerImpl::new); // Stateless

server.listenAndServe() // Blocking: returns only after a call to server.close()
```

> *Note*
>
> For your factories and receivers, you can either use real instances, lambdas 
> or method references.
> As the server uses multiple threads to handle its clients, make sure that
> the receivers are thread-safe, especially in a stateful API.

### Logging Management

When unexpected exceptions occur while running a server, they are logged using
[Java Logging API](https://docs.oracle.com/javase/8/docs/technotes/guides/logging/overview.html).

Because you may want to manage yourself the logging, you can get access to the
logger by calling the following method.

```java
Logger athenaLogger = Logger.getLogger("athena");
```

Then, all you have to do is to set the parameters that fit your needs (e.g. 
log level, handlers, etc.).

## Some recommendations

This section sums up all what is important to know in order to use Athena.

1. All the custom objects that are exchanged through RPC must implement
   `Serializable`.
   Don't forget to specify a **non-default** `serialVersionUID` for these
   classes!

2. You should surround each remote call in a `try-catch` block to catch an
   `RpcTreatmentException`.
   Since this is an unchecked exception, this is not required.
   It is however a best practice: you can, for example, write your own 
   **decorator** for the stub, which will handle the exceptions.

3. Make sure that both the client and the server know the same receiver
   interfaces and the same exchanged classes, to ensure that the requests 
   and their responses can be properly serialized.

4. Because the exceptions thrown on the server are propagated to the client,
   make sure that no sensitive data can be accessed this way.

5. The communication between the client and the server is **not secured**.
   So, make sure that no sensitive data is exchanged during remote calls.
